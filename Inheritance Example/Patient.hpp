//
//  Patient.hpp
//  Inheritance Example
//
//  Created by Lee Barney on 2/25/19.
//  Copyright © 2019 Lee Barney. All rights reserved.
//

#ifndef Patient_hpp
#define Patient_hpp

#include <string>

class Treatment;
#include "Person.hpp"
using namespace std;
//is-a relationship
class Patient:public Person {
private:
    //has-a relationships
    Treatment* treatments;
    int maxTreatmentCount;
    int numTreatments;
    string insuranceNumber;
public:
    //constructors
    Patient():insuranceNumber(""),numTreatments(0),maxTreatmentCount(0),treatments(NULL),Person(){}
    Patient(const string& anInsuranceNumber, const string& anId,
            const string& aFirstName, const string& aLastName, int anAge):insuranceNumber(anInsuranceNumber),numTreatments(0),treatments(NULL),Person(anId,aFirstName,aLastName,anAge){}
    //getters and setters
    const Treatment* getTreatments()const;
    void setInsuranceNumber(const string& anInsuranceNumber);
    const string& getInsuranceNumber()const;
    //add methods
    void addTreatment(const Treatment& aTreatment);
    void addTreatments(const Treatment* someTreatmentsToAdd, int numTreatmentsToAdd);
    
    ~Patient();
};

#endif /* Patient_hpp */
