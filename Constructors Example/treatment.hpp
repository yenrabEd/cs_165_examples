//
//  treatment.hpp
//  contstructors example
//
//  Created by Lee Barney on 2/1/19.
//  Copyright © 2019 Lee Barney. All rights reserved.
//

#ifndef treatment_hpp
#define treatment_hpp

#include <stdio.h>
#include <string>
using namespace std;

class Treatment{
private:
    string patientID;
    int treatmentType;
    
public:
    //Constructors
    Treatment();
    Treatment(const string& anID, int aType);
    Treatment(const Treatment& aTreatment);
    
    //getters and setters
    string getPatientID();
    void setPatientID(string anID);
    
    int getTreatmentType();
    void setTreatmentType(int aType);
};

#endif /* treatment_hpp */
