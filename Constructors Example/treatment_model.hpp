//
//  read_write.hpp
//  contstructors example
//
//  Created by Lee Barney on 2/1/19.
//  Copyright © 2019 Lee Barney. All rights reserved.
//

#ifndef treatment_model_hpp
#define treatment_model_hpp

#include <stdio.h>
#include <string>
#include "treatment.hpp"

using namespace std;

class TreatmentModel {
private:
    string fileName;
    Treatment* theTreatments;
    int maxTreatmentCount;
    int treatmentsReadIn;
    
    //private method
    void resizeArray() throw (const char*);
public:
    //constructors
    TreatmentModel() throw (const char*);
    TreatmentModel(string treatmentsFileName) throw (const char*);
    TreatmentModel(string treatmentsFileName, int anticipatedSize) throw (const char*);
    void setFileName(string aFileName);
    string getFilename();
    void setMaxTreatmentCount(int aTreatmentCount);
    int getMaxTreatmentCount();
    
    int getTreatmentCount();
    
    const Treatment* getTreatments();
    
    void loadTreatments();
    void writeTreatments();
    
    void addTreatment(const Treatment& aTreatment);
    
    Treatment* getTreatmentsByType(int aType);
    Treatment* getTreatmentsByPatient(const string& aPatientID);
    
};

#endif /* treatment_model_hpp */
