//
//  read_write.cpp
//  contstructors example
//
//  Created by Lee Barney on 2/1/19.
//  Copyright © 2019 Lee Barney. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <sstream>
#include "treatment_model.hpp"



TreatmentModel::TreatmentModel() throw (const char*):fileName("all_treatments"),theTreatments(NULL),maxTreatmentCount(0),treatmentsReadIn(0){
    resizeArray();
}
TreatmentModel::TreatmentModel(string treatmentsFileName) throw (const char*):fileName(treatmentsFileName),theTreatments(NULL),maxTreatmentCount(0),treatmentsReadIn(0){
    resizeArray();
}
TreatmentModel::TreatmentModel(string treatmentsFileName, int anticipatedSize) throw (const char*):fileName(treatmentsFileName),maxTreatmentCount(anticipatedSize),treatmentsReadIn(0){
    theTreatments = (Treatment*) malloc(maxTreatmentCount);
}


void TreatmentModel::setFileName(string aFileName){
    fileName = aFileName;
}
string TreatmentModel::getFilename(){
    return fileName;
}
void TreatmentModel::setMaxTreatmentCount(int aTreatmentCount){
    maxTreatmentCount = aTreatmentCount;
}
int TreatmentModel::getMaxTreatmentCount(){
    return maxTreatmentCount;
}

int TreatmentModel::getTreatmentCount(){
    return treatmentsReadIn;
}
const Treatment* TreatmentModel::getTreatments(){
    return theTreatments;
}

void TreatmentModel::loadTreatments(){
    if (fileName.length()!=0) {
        if (theTreatments != NULL) {
            //clean up and reset
            free(theTreatments);
            theTreatments = NULL;
            maxTreatmentCount = 0;
            resizeArray();
        }
        //open the file with the treatment descriptions in it
        ifstream infile(fileName);
        
        //read data from the file line by line
        string lineFromFile;
        while (getline(infile, lineFromFile)){
            //setup to parse the line
            string aPatientId;
            int aTreatmentType;
            //read each part of description from the line
            istringstream aStringStream(lineFromFile);
            
            if (!(aStringStream >> aPatientId >> aTreatmentType)){ exit(1); } // if you find bad data exit the app with an error indicator
            Treatment aTreatment(aPatientId,aTreatmentType);
            addTreatment(aTreatment);
        }
    }
}

void TreatmentModel::addTreatment(const Treatment& aTreatment){
    theTreatments[treatmentsReadIn] = aTreatment;
    treatmentsReadIn++;
    //if the array is full, resizeArray it
    if (treatmentsReadIn == maxTreatmentCount) {
        resizeArray();
    }
}

void TreatmentModel::writeTreatments(){
    if (theTreatments != NULL) {
        //think about how you might do this.
    }
}

Treatment* TreatmentModel::getTreatmentsByType(int aType){
    //think about how you might do this
    return NULL;//placeholder
}
Treatment* TreatmentModel::getTreatmentsByPatient(const string& aPatientID){
    //think about how you might do this
    return NULL;//placeholder
}



//Really this should be done using templating instead of hard-coding the type to be Treatment. That way the type of thing in the array could be anything.
//You will see how to do templating later.
void TreatmentModel::resizeArray() throw (const char*){
    if(maxTreatmentCount < 0){
        throw "Error: Unable to resize negative-sized arrays.";
    }
    int adjustedArrayCount = maxTreatmentCount > 0 ? maxTreatmentCount*2 : 2;
    int adjustedArraySize = adjustedArrayCount*sizeof(Treatment);
    //allocate the memory for the next array
    Treatment* adjustedArray = (Treatment*) malloc(adjustedArraySize);
    if(adjustedArray == NULL){
        throw "Error: Memory allocation error. You need to close other applications. You have run out of memory.";
    }
    //clear out any data hanging around from the last time this memory was used (this will keep dealocation crashes from happening)
    memset(adjustedArray, 0, adjustedArraySize);
    if (maxTreatmentCount > 0) {
        //Copy the contents of the current array to the new one.
        memcpy(adjustedArray, theTreatments, maxTreatmentCount*sizeof(Treatment));
        //free up the old array so there are no memory leaks
        free(theTreatments);
    }
    maxTreatmentCount = adjustedArrayCount;
    theTreatments = adjustedArray;
}
