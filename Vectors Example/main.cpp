//
//  main.cpp
//  Vectors Example
//
/*  Created by Lee Barney on 3/9/19.
 *  Copyright (c) 2018 Lee Barney
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

#include <iostream>
#include <algorithm>
#include "treatment.hpp"
#include "model.hpp"
#include "treatment_builder.hpp"
#include "json_builder.hpp"
#include "csv_builder.hpp"



//compares two Treatment id's.
//used for sorting
bool compareTreatmentIDs(const Treatment& aTreatment, const Treatment& anotherTreatment){
    return aTreatment.getPatientID().compare(anotherTreatment.getPatientID());
}
//compares two Treatment types.
//used for sorting
bool compareTreatmentTypes(const Treatment& aTreatment, const Treatment& anotherTreatment){
    return aTreatment.getTreatmentType() < anotherTreatment.getTreatmentType();
}

//checks single Treatment for matching the filter
bool filterTypeLessThan(Treatment aTreatment){
    return aTreatment.getTreatmentType() <= 9;
}

//loads data into the model
void loadData(Model<Treatment>& theModel, string theFileName);


int main(int argc, const char * argv[]) {
    //Instantiate the DisplayBuilders
    JsonBuilder theJsonBuilder;
    CsvBuilder theCsvBuilder;
    //load an example example set of Treatments
    Model<Treatment> aModel;
    loadData(aModel,"first_example_file");
    
    cout<<"Sorted by ID"<<endl;
    vector<Treatment> allTreatments = aModel.getData();
    std::sort(allTreatments.begin(),allTreatments.end(),compareTreatmentIDs);
    
    string asJSON = theJsonBuilder.buildDisplay(allTreatments);
    cout<<asJSON<<endl;
    
    //now load a different example set
    Model<Treatment> anotherModel;
    cout<<endl<<endl<<"Sorted by type"<<endl;
    
    loadData(anotherModel,"second_example_file");
    allTreatments = anotherModel.getData();
    std::sort(allTreatments.begin(), allTreatments.end(), compareTreatmentTypes);
    string asCSV = theCsvBuilder.buildDisplay(allTreatments);
    cout<<asCSV<<endl;
    //Filter the data from the model
    vector<Treatment> filteredData;
    cout<<endl<<endl<<"Filtered and sorted by type"<<endl;
    std::copy_if(allTreatments.begin(), allTreatments.end(), std::back_inserter(filteredData), filterTypeLessThan);
    asJSON = theJsonBuilder.buildDisplay(filteredData);
    
    cout<<asJSON<<endl;
    return 0;
}

void loadData(Model<Treatment>& theModel, string theFileName){
    if (theFileName.length()!=0) {
        theModel.clear();
        //open the file with the treatment descriptions in it
        ifstream infile(theFileName);
        
        //read data from the file line by line
        string lineFromFile;
        while (getline(infile, lineFromFile)){
            //read each part of description from the line
            Treatment aTreatment = TreatmentBuilder::buildFrom(lineFromFile);
            theModel.addData(aTreatment);
        }
    }
}

void writeData(){
    //Think about how you might do this.
}
