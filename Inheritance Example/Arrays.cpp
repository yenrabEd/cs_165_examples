//
//  Arrays.cpp
//  Static
//
//  Created by Lee Barney on 2/15/19.
//  Copyright © 2019 Lee Barney. All rights reserved.
//

#include "Arrays.hpp"
#include "Treatment.hpp"

Treatment* Arrays::sort(Treatment* arrayToSort, int arraySize, treatment_comparitor_function aComparitor){
    //Bubble Sort implementation
    int i, j;
    for (i = 0; i < arraySize-1; i++){
        // Last i elements are already in place
        for (j = 0; j < arraySize-i-1; j++){
            if (aComparitor(&arrayToSort[j], &arrayToSort[j+1])){
                Treatment temp = arrayToSort[j];
                arrayToSort[j] = arrayToSort[j+1];
                arrayToSort[j+1] = temp;
            }
        }
    }
    return arrayToSort;
}
//Lots of code duplication here.
//Later in the class you will learn how to make
//one of these generic so it can handle any type
//of elements in the array.
int* Arrays::sort(int* arrayToSort, int arraySize, int_comparitor_function aComparitor){
    //Bubble Sort implementation
    int i, j;
    for (i = 0; i < arraySize; i++){
        // Last i elements are already in place
        for (j = 0; j < arraySize-i-1; j++){
            if (aComparitor(arrayToSort[j], arrayToSort[j+1])){
                int temp = arrayToSort[j];
                arrayToSort[j] = arrayToSort[j+1];
                arrayToSort[j+1] = temp;
            }
        }
    }
    return arrayToSort;
}

double* Arrays::sort(double* arrayToSort, int arraySize, double_comparitor_function aComparitor){
    //Bubble Sort implementation
    int i, j;
    for (i = 0; i < arraySize; i++){
        // Last i elements are already in place
        for (j = 0; j < arraySize-i-1; j++){
            if (aComparitor(arrayToSort[j], arrayToSort[j+1])){
                double temp = arrayToSort[j];
                arrayToSort[j] = arrayToSort[j+1];
                arrayToSort[j+1] = temp;
            }
        }
    }
    return arrayToSort;
}

//Really this should be done using templating instead of hard-coding the type to be Treatment. That way the type of thing in the array could be anything.
//You will see how to do templating later.
void Arrays::resize(int currentSize, Treatment* theTreatments) throw (const char*, ){
    if(currentSize < 0){
        throw "Error: Unable to resize negative-sized arrays.";
    }
    int adjustedArrayCount = currentSize > 0 ? currentSize*2 : 2;
    int adjustedArraySize = adjustedArrayCount*sizeof(Treatment);
    //allocate the memory for the next array
    Treatment* adjustedArray = (Treatment*) malloc(adjustedArraySize);
    if(adjustedArray == NULL){
        throw "Error: Memory allocation error. You need to close other applications. You have run out of memory.";
    }
    //clear out any data hanging around from the last time this memory was used (this will keep dealocation crashes from happening)
    memset(adjustedArray, 0, adjustedArraySize);
    if (currentSize > 0) {
        //Copy the contents of the current array to the new one.
        memcpy(adjustedArray, theTreatments, currentSize*sizeof(Treatment));
        //free up the old array so there are no memory leaks
        free(theTreatments);
    }
    currentSize = adjustedArrayCount;
    theTreatments = adjustedArray;
}
