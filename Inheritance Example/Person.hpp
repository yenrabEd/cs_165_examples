//
//  Person.hpp
//  Inheritance Example
//
//  Created by Lee Barney on 2/25/19.
//  Copyright © 2019 Lee Barney. All rights reserved.
//

#ifndef Person_hpp
#define Person_hpp

#include <string>
using namespace std;
class Person{
private:
    string id;
    string firstName;
    string lastName;
    int age;
public:
    
    //Constructors
    Person():id(""), firstName(""), lastName(" "), age(-1){}
    Person(string anId, string aFirstName, string aLastName, int anAge):
    id(anId),firstName(aFirstName),lastName(aLastName),age(anAge){}
    //Accessors and Mutators
    void setId(const string& anId);
    const string& getId() const;
    void setFirstName(const string& aFirstName);
    const string& getFirstName() const;
    void setLastName(const string& aLastName);
    const string& getLastName() const;
    void setAge(int anAge);
    int getAge();
    
    //destructor
    ~Person(){}
    
};

#endif /* Person_hpp */
