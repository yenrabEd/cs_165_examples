//
//  main.cpp
//  contstructors example
//
//  Created by Lee Barney on 2/1/19.
//  Copyright © 2019 Lee Barney. All rights reserved.
//

#include <iostream>
#include "treatment_model.hpp"

using namespace std;

int main(int argc, const char * argv[]) {
    //load an example example set
    TreatmentModel aModel;
    aModel.setFileName("first_example_file");
    aModel.loadTreatments();
    
    const Treatment* theTreatments = aModel.getTreatments();
    for (int i = 0; i<aModel.getTreatmentCount(); i++) {
        Treatment aTreatment = theTreatments[i];
        cout<<"ID: "<<aTreatment.getPatientID()<<" type: "<<aTreatment.getTreatmentType()<<endl;
    }
    
    cout<<endl<<endl<<"Next set"<<endl;
    
    
    //now load a different example set
    TreatmentModel anotherModel("second_example_file",5);
    anotherModel.loadTreatments();
    

    const Treatment* otherTreatments = anotherModel.getTreatments();
    //some duplicate code here...what does that mean we should do????
    for (int i = 0; i<anotherModel.getTreatmentCount(); i++) {
        Treatment aTreatment = otherTreatments[i];
        cout<<"ID: "<<aTreatment.getPatientID()<<" type: "<<aTreatment.getTreatmentType()<<endl;
    }
    
    return 0;
}

