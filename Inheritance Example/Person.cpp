//
//  Person.cpp
//  Inheritance Example
//
//  Created by Lee Barney on 2/25/19.
//  Copyright © 2019 Lee Barney. All rights reserved.
//

#include "Person.hpp"

void Person::setId(const string& anId){
    id = anId;
}
const string& Person::getId()const{
    return id;
}
void Person::setFirstName(const string& aFirstName){
    firstName = aFirstName;
}
const string& Person::getFirstName()const{
    return firstName;
}
void Person::setLastName(const string& aLastName){
    lastName = aLastName;
}
const string& Person::getLastName()const{
    return lastName;
}
void Person::setAge(int anAge){
    age = anAge;
}
int Person::getAge(){
    return age;
}
