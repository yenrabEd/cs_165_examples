//
//  main.cpp
//  Structs
//
/*  Copyright (c) 2018 Lee Barney
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

#include <iostream>
#include "Structs.hpp"
#include "ExampleFuncs.hpp"

int main(int argc, const char * argv[]) {
    //Ways to intitialize a struct
    Student president;
    president.firstName="Sally";
    president.lastName="Hernandez";
    president.classStanding=3;
    president.iNumber = 12345689123;
    
    //Order doesn't matter
    Student vicePresident;
    vicePresident.iNumber=5432432154321;
    vicePresident.firstName="Joe";
    vicePresident.lastName="Habermawitz";
    vicePresident.classStanding=2;
    
    //Order matters
    Student secretary = {
        "Bob",
        "So",
        987657643542,
        1
    };
    //ititializing struct arrays
    Student programmingLeadership[3] = {president,vicePresident,secretary};
    
    Student engineeringLeadership[3] = {{"Jeong Soon","So",5432542,3},{"Jane","Austin",5432890,3},{"Sam","Jones",195432543,1}};
    
    //stuct-ception!!!
    Leadership betterDesignProgrammingLeadership = {president,vicePresident,secretary};
    Leadership betterDesignEngineeringLeadership = {
        {"Jeong Soon","So",5432542,3},
        {"Jane","Austin",5432890,3},
        {"Sam","Jones",195432543,1}
    };
    
    /*Getting the stored data back out*/
    
    //directly
    cout<<vicePresident.firstName<<" "<<vicePresident.lastName<<endl;
    
    //from array
    cout<<engineeringLeadership[0].firstName<<" "<<engineeringLeadership[0].lastName<<endl;
    
    //from struct-ception
    cout<<betterDesignEngineeringLeadership.president.firstName<<" "<<betterDesignEngineeringLeadership.president.lastName<<endl;
    
    //passing a struct
    Student aStudent;
    fillStudent(aStudent);
    cout<<"Name: "<<aStudent.firstName<<" "<<aStudent.lastName
        <<endl<<"\tclass standing: "<<aStudent.classStanding
        <<endl<<"\tiNumber: "<<aStudent.iNumber<<endl;
    Student anotherStudent;
    badFillStudent(anotherStudent);
    
    cout<<"Name: "<<anotherStudent.firstName<<" "<<anotherStudent.lastName
    <<endl<<"\tclass standing: "<<anotherStudent.classStanding
    <<endl<<"\tiNumber: "<<anotherStudent.iNumber<<endl;
    
    cout<<betterDesignEngineeringLeadership.president.iNumber<<endl;
    
    return 0;
    
}
