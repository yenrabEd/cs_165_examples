//
//  Arrays.hpp
//  Static
//
//  Created by Lee Barney on 2/15/19.
//  Copyright © 2019 Lee Barney. All rights reserved.
//

#ifndef Arrays_hpp
#define Arrays_hpp
class Treatment;


//function pointers
typedef bool (*treatment_comparitor_function)(Treatment* firstTreatment, Treatment* secondTreatment);
typedef bool (*int_comparitor_function)(int firstInt, int secondInt);
typedef bool (*double_comparitor_function)(double firstDouble, double secondDouble);

class Arrays {
public:
    //Sorting for specific types shown here.
    //Later in the class you will learn how to do this generically.
    static int* sort(int* arrayToSort, int arraySize, int_comparitor_function aFunction);
    static double* sort(double* arrayToSort, int arraySize, double_comparitor_function aFunction);
    static Treatment* sort(Treatment* arrayToSort, int arraySize, treatment_comparitor_function aFunction);
    static void resize(int currentSize, Treatment* theTreatments) throw (const char*);
};

#endif /* Arrays_hpp */
