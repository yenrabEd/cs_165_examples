//
//  main.cpp
//  Inheritance Example
//
//  Created by Lee Barney on 2/25/19.
//  Copyright © 2019 Lee Barney. All rights reserved.
//

#include <iostream>
#include "Patient.hpp"
using namespace std;

int main(int argc, const char * argv[]) {
    /* I have left the treatment_model class out of
     * this example for the purpose of clarity.
     * In 'the real world' it would be included and used.
     */
    Patient aPatient = Patient("i23bxA", "p100:34", "Sally", "Jimenez", 14);
    cout<<"Patient: "<<aPatient.getFirstName()<<" "<<aPatient.getLastName()
        <<" is "<<aPatient.getAge()<<" years old."<<endl;
    return 0;
}
