//
//  treatment.cpp
//  contstructors example
//
//  Created by Lee Barney on 2/1/19.
//  Copyright © 2019 Lee Barney. All rights reserved.
//

#include "treatment.hpp"

//getters and setters
string Treatment::getPatientID(){
    return patientID;
}
void Treatment::setPatientID(string anID){
    patientID = anID;
}

int Treatment::getTreatmentType(){
    return treatmentType;
}
void Treatment::setTreatmentType(int aType){
    treatmentType = aType;
}

//constructors

/*
 * old fashioned constructor
 Treatment::Treatment(){
    patientID = "";
    treatmentType = -1;
 }
 */
//modern constructor
Treatment::Treatment():patientID(""),treatmentType(-1){}

/*
 * old fashoined constructor
 
 Treatment::Treatment(const string& anID, int aType){
    patientID = anId;
    treatmentType = aType;
 }
 */
//modern constructor
Treatment::Treatment(const string& anID, int aType):patientID(anID),treatmentType(aType){}

//copy constructor
Treatment::Treatment(const Treatment& treatmentToCopy){
    patientID = treatmentToCopy.patientID;
    treatmentType = treatmentToCopy.treatmentType;
}
