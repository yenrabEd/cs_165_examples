//
//  ExampleFuncs.cpp
//  Structs
//
//  Created by Lee Barney on 1/14/19.
/*  Copyright (c) 2018 Lee Barney
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

#include <iostream>

#include "ExampleFuncs.hpp"
#include "Structs.hpp"

using namespace std;

//function definition
void fillStudent(Student& aStudent){
    /*
     * This code is non-optimal.
     * How might you make this code safer
     * so that the user couldn't
     * make data entry mistakes?
     */	
    cout<<"Enter Student First Name: ";
    cin>>aStudent.firstName;
    cout<<"Enter Student Last Name: ";
    cin>>aStudent.lastName;
    cout<<"Enter Student iNumber: ";
    cin>>aStudent.iNumber;
    cout<<"Enter Freshman(0), Sophmore(1), Junior(2), or Senior(3): ";
    cin>>aStudent.classStanding;
}

void badFillStudent(Student aStudent){
    cout<<"Enter Student First Name: ";
    cin>>aStudent.firstName;
    cout<<"Enter Student Last Name: ";
    cin>>aStudent.lastName;
    cout<<"Enter Student iNumber: ";
    cin>>aStudent.iNumber;
    cout<<"Enter Freshman(0), Sophmore(1), Junior(2), or Senior(3): ";
    cin>>aStudent.classStanding;
}
