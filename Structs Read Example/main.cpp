//
//  main.cpp
//  Struct_Read
//
/*  Created by Lee Barney on 1/16/19.
*  Copyright (c) 2018 Lee Barney
*  Permission is hereby granted, free of charge, to any person obtaining a
*  copy of this software and associated documentation files (the "Software"),
*  to deal in the Software without restriction, including without limitation the
*  rights to use, copy, modify, merge, publish, distribute, sublicense,
*  and/or sell copies of the Software, and to permit persons to whom the
*  Software is furnished to do so, subject to the following conditions:

*  The above copyright notice and this permission notice shall be
*  included in all copies or substantial portions of the Software.
*
*  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
*  SOFTWARE.
*/

#include <iostream>
#include <fstream>
#include <sstream>

#include "Structs.hpp"

using namespace std;

//forward declaration
Student* resizeArray(Student* currentArray, int& currentArrayCount) throw (const char*);

int main(int argc, const char * argv[]) {
    int possibleLeadersArrayCount = 0;
    try {
        Student* possibleLeaders = resizeArray(NULL, possibleLeadersArrayCount);
        //keep track of how many have been read in from the file
        int numPossibleLeadersReadIn = 0;
        //open the file with the student descriptions in it
        ifstream infile("students.data.stuff");
        
        //read data from the file line by line
        string lineFromFile;
        while (getline(infile, lineFromFile)){
            //setup to parse the line
            string aFirstName,aLastName;
            long anINumber;
            int aClassStanding;
            //read each part of description from the line
            istringstream aStringStream(lineFromFile);
            
            if (!(aStringStream >> aFirstName >> aLastName >> anINumber >> aClassStanding)) { exit(1); } // if you find bad data exit the app with an error indicator
            Student aStudent = {aFirstName,aLastName,anINumber,aClassStanding};
            possibleLeaders[numPossibleLeadersReadIn] = aStudent;
            numPossibleLeadersReadIn++;
            //if the array is full, resizeArray it
            if (numPossibleLeadersReadIn == possibleLeadersArrayCount) {
                possibleLeaders = resizeArray(possibleLeaders, possibleLeadersArrayCount);
            }
        }
        
        //Just to test the results of filling the array, print out the first and
        //last element of the array to see if they are correct.
        Student firstLeader = possibleLeaders[0];
        Student lastLeader = possibleLeaders[numPossibleLeadersReadIn-1];
        cout<<"first leader: "<<firstLeader.firstName<<" "<<firstLeader.lastName<<" "
        <<firstLeader.iNumber<<" "<<firstLeader.classStanding<<endl;
        
        cout<<"last leader: "<<lastLeader.firstName<<" "<<lastLeader.lastName<<" "
        <<lastLeader.iNumber<<" "<<lastLeader.classStanding<<endl;
    } catch (const char* message) {
        cout<<message<<endl;
        exit(1);
    }
    
    
    try {
        cout<<endl<<endl<<"About to try fake array"<<endl;
        int fakeArraySize = -4;
        resizeArray(NULL, fakeArraySize);
        cout<<"After resize"<<endl;
    } catch (const char* message) {
        cout<<message<<endl;
    }
    
    
    
    return 0;
}

//Really this should be done using templating instead of hard-coding the type to be Student. That way the type of thing in the array could be anything.
//You will see how to do templating later.
Student* resizeArray(Student* currentArray, int& currentArrayCount) throw (const char*){
    if(currentArrayCount < 0){
        throw "Error: Unable to resize negative-sized arrays.";
    }
    int adjustedArrayCount = currentArrayCount > 0 ? currentArrayCount*2 : 2;
    int adjustedArraySize = adjustedArrayCount*sizeof(Student);
    //allocate the memory for the next array
    Student* adjustedArray = (Student*) malloc(adjustedArraySize);
    if(adjustedArray == NULL){
        throw "Error: Memory allocation error. You need to close other applications. You have run out of memory.";
    }
    //clear out any data hanging around from the last time this memory was used (this will keep dealocation crashes from happening)
    memset(adjustedArray, 0, adjustedArraySize);
    if (currentArrayCount > 0) {
        //Copy the contents of the current array to the new one.
       memcpy(adjustedArray, currentArray, currentArrayCount*sizeof(Student));
        //free up the old array so there are no memory leaks
        free(currentArray);
    }
    currentArrayCount = adjustedArrayCount;
    return adjustedArray;
}



