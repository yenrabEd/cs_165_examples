//
//  Baby.hpp
//  Encapsulation Example
//
//  Created by Lee Barney on 1/28/19.
/*  Copyright (c) 2018 Lee Barney
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

#ifndef Baby_hpp
#define Baby_hpp

using namespace std;

/*
 *The code in this header file is similar to
 *forward declaring functions in your previous
 *assignments.
 *
 *You are 'forward declaring' the Baby class
 *all at once.
*/
class Baby{
    //Attributes should be private
private:
    string name;
    float weight;
    
    //Methods should be public
public:
    string getName() const;
    void setName(string aName) throw (const char*);
    
    float getWeight();
    void setWeight(float aWeight);
    
};

#endif /* Baby_hpp */
