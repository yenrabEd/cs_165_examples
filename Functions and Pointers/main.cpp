//
//  main.cpp
//  165Examples
//
//  Created by Lee Barney on 1/9/19.
/*  Copyright (c) 2018 Lee Barney
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

#include <iostream>

#include "ExampleFuncs.hpp"

using namespace std;

//forward declarations
int factLoop(int aNumber);
//int fact(int aNumber);
int averageHighGPAs(double* arrayOfGPAs, int arrayLength, double* average);


int main(int argc, const char * argv[]) {
    double GPAs[7]={3.2,3.13,1.57,0.2,4.0,3.8,1.2};
    double theAverage= -1.0;//default value
    int numAveraged = averageHighGPAs(GPAs,7,&theAverage);
    cout<<"The average of "<<numAveraged<<" GPA's is "<<theAverage<<endl;
    dynamicArray();
    return 0;
}

//why return the count of ints averaged????
int averageHighGPAs(double* arrayOfGPAs, int arrayLength, double* average){
    double sum = 0;
    int counter = 0;
    for (int i = 0; i<arrayLength; i++) {
        double aGPA = arrayOfGPAs[i];
        if (aGPA < 2.7) {
            continue;
        }
        sum += aGPA;
        counter++;
    }
    *average = sum/counter;
    return counter;
}

int factLoop(int aNumber){
    int result = 1;
    while (aNumber > 0) {
        result *= aNumber;
        aNumber--;
    }
    return result;
}

