//
//  treatment.cpp
//  contstructors example
//
/*  Created by Lee Barney on 1/16/19.
 *  Copyright (c) 2018 Lee Barney
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

#include "treatment.hpp"

//getters and setters
string Treatment::getPatientID() const{
    return patientID;
}
void Treatment::setPatientID(string anID){
    patientID = anID;
}

int Treatment::getTreatmentType() const{
    return treatmentType;
}
void Treatment::setTreatmentType(int aType){
    treatmentType = aType;
}

//constructors

/*
 * old fashioned constructor
 Treatment::Treatment(){
    patientID = "";
    treatmentType = -1;
 }
 */
//modern constructor
Treatment::Treatment():patientID(""),treatmentType(-1){}

/*
 * old fashoined constructor
 
 Treatment::Treatment(const string& anID, int aType){
    patientID = anId;
    treatmentType = aType;
 }
 */
//modern constructor
Treatment::Treatment(const string& anID, int aType):patientID(anID),treatmentType(aType){}

//copy constructor
Treatment::Treatment(const Treatment& treatmentToCopy){
    patientID = treatmentToCopy.patientID;
    treatmentType = treatmentToCopy.treatmentType;
}
