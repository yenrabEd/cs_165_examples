//
//  main.cpp
//  Operator Overloading
//
//  Created by Lee Barney on 3/25/19.
/*  Copyright (c) 2018 Lee Barney
*  Permission is hereby granted, free of charge, to any person obtaining a
*  copy of this software and associated documentation files (the "Software"),
*  to deal in the Software without restriction, including without limitation the
*  rights to use, copy, modify, merge, publish, distribute, sublicense,
*  and/or sell copies of the Software, and to permit persons to whom the
*  Software is furnished to do so, subject to the following conditions:

*  The above copyright notice and this permission notice shall be
*  included in all copies or substantial portions of the Software.
*
*  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
*  SOFTWARE.
*/

#include <iostream>
#include "treatment.hpp"
#include "tools.hpp"

int main(int argc, const char * argv[]) {
    
    Treatment someTreatment("retr43gnt6",0);
    Treatment otherTreatment("bo50234",1);
    
    //using the << syntactic sugar
    cout<<someTreatment<<" "<<otherTreatment<<endl;
    
    //checking for equality
    cout<<"Equal?? "<<(someTreatment == otherTreatment)<<endl;
    cout<<"Equal?? "<<(someTreatment == someTreatment)<<endl;
    
    
    //checking for equivalence
    Treatment thirdTreatment("retr43gnt6",0);
    cout<<"Equivalent?? "<<(someTreatment == someTreatment)<<endl;
    cout<<"Equivalent?? "<<(someTreatment == otherTreatment)<<endl;
    cout<<"Equivalent?? "<<(someTreatment == thirdTreatment)<<endl;
    return 0;
}
