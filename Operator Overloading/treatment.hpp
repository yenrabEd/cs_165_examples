//
//  treatment.hpp
//  contstructors example
//
/*  Created by Lee Barney on 3/9/19.
 *  Copyright (c) 2018 Lee Barney
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

#ifndef treatment_hpp
#define treatment_hpp

#include <stdio.h>
#include <string>
using namespace std;

class Treatment{
private:
    string patientID;
    int treatmentType;
    
public:
    //Constructors
    Treatment();
    Treatment(const string& anID, int aType);
    Treatment(const Treatment& aTreatment);
    
    //getters and setters
    string getPatientID() const;
    void setPatientID(string anID);
    
    int getTreatmentType() const;
    void setTreatmentType(int aType);
    
    bool operator ==(const Treatment& aTreatment){
        return &aTreatment == this
                || (this->treatmentType == aTreatment.treatmentType
                && this->patientID.compare(aTreatment.patientID)==0);
    }
};

#endif /* treatment_hpp */
