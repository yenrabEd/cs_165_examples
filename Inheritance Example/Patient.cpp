//
//  Patient.cpp
//  Inheritance Example
//
//  Created by Lee Barney on 2/25/19.
//  Copyright © 2019 Lee Barney. All rights reserved.
//

#include "Patient.hpp"
#include "Arrays.hpp"
#include "Treatment.hpp"

const Treatment* Patient::getTreatments()const{
    return treatments;
}
void Patient::setInsuranceNumber(const string& anInsuranceNumber){
    insuranceNumber = anInsuranceNumber;
}
const string& Patient::getInsuranceNumber()const{
    return insuranceNumber;
}
//add methods
void Patient::addTreatment(const Treatment& aTreatment){
    if (numTreatments >= maxTreatmentCount) {
        Arrays::resize(numTreatments,treatments);
    }
    treatments[numTreatments] = aTreatment;
}


//this is a façade function
void Patient::addTreatments(const Treatment* someTreatmentsToAdd, int numTreatmentsToAdd){
    for (int i = 0; i < numTreatmentsToAdd; i++) {
        addTreatment(someTreatmentsToAdd[i]);
    }
}

//destructor
Patient::~Patient(){
    free(treatments);
}
